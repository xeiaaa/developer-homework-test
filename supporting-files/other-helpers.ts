import { ConvertUnits } from "./helpers"
import { UnitOfMeasure, UoMName, UoMType } from "./models"

/*
  There is no CUPS to KILOGRAM inside UnitsConversionTable
  But we can convert
    cups -> millilitres
    millilitres -> grams
    grams -> kilograms
*/
export function CupsToKilogram (
  fromUoM: UnitOfMeasure,
) {
  const millilitres = ConvertUnits(fromUoM, UoMName.millilitres, UoMType.volume)
  const grams = ConvertUnits(millilitres, UoMName.grams, UoMType.mass)
  return ConvertUnits(grams, UoMName.kilogram, UoMType.mass)
}