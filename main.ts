import {
    GetProductsForIngredient,
    GetRecipes,
} from "./supporting-files/data-access";
import {
    Product,
    SupplierProduct,
    UnitOfMeasure,
    UoMName,
} from "./supporting-files/models";
import {
    GetCostPerBaseUnit,
    GetNutrientFactInBaseUnits,
    ConvertUnits,
} from "./supporting-files/helpers";
import { RunTest, ExpectedRecipeSummary } from "./supporting-files/testing";
import { CupsToKilogram } from "./supporting-files/other-helpers";

console.clear();
console.log("Expected Result Is:", ExpectedRecipeSummary);

const recipeData = GetRecipes(); // the list of 1 recipe you should calculate the information for
const recipeSummary: any = {}; // the final result to pass into the test function

/*
 * YOUR CODE GOES BELOW THIS, DO NOT MODIFY ABOVE
 * (You can add more imports if needed)
 * */

const [cremeBrulee] = recipeData;
const { lineItems } = cremeBrulee;

/*
    We are interested in 2 things.
    1) Lowest price needed for an ingredient
    2) Nutrient Facts of the product where the Supplier Product with the lowest cost is found
*/
const ingredientMap = lineItems.map((lineItem) => {
    const products = GetProductsForIngredient(lineItem.ingredient);

    //  Needed to calculate priceNeededForIngredient
    let leastCostPerBaseUnit: number = GetCostPerBaseUnit(
        products[0].supplierProducts[0]
    );

    /*
        Needed to get the supplierProductUoM for conversion since ingredientUom is
        sometimes not equal to supplierProductUom
    */
    let lowestSupplierProduct: SupplierProduct =
        products[0].supplierProducts[0];

    //  Product where the Supplier Product with the lowest cost is found
    let productWithTheLowestSupply: Product = products[0];

    /*
        Opted for a for loop since we need to track 3 things in a single go.
        Could have used a combination of Math.min and Array.prototype.map to get the
        `leastCostPerBaseUnit`, but we'll still needed to loop to get the other 2 values.
    */
    for (let product of products) {
        for (let supplierProduct of product.supplierProducts) {
            const currentCosePerBaseUnit = GetCostPerBaseUnit(supplierProduct);
            if (currentCosePerBaseUnit < leastCostPerBaseUnit) {
                leastCostPerBaseUnit = currentCosePerBaseUnit;
                lowestSupplierProduct = supplierProduct;
                productWithTheLowestSupply = product;
            }
        }
    }

    // Convert ingredientUom as supplierProductUom in order to calculate the needed price
    let converted: UnitOfMeasure;
    if (
        lineItem.unitOfMeasure.uomName === UoMName.cups &&
        lowestSupplierProduct.supplierProductUoM.uomName === UoMName.kilogram
    ) {
        converted = CupsToKilogram(lineItem.unitOfMeasure);
    } else {
        converted = ConvertUnits(
            lineItem.unitOfMeasure,
            lowestSupplierProduct.supplierProductUoM.uomName,
            lowestSupplierProduct.supplierProductUoM.uomType
        );
    }

    const priceNeededForIngredient =
        lineItem.unitOfMeasure.uomAmount * leastCostPerBaseUnit;

    return {
        nutrientFacts: productWithTheLowestSupply.nutrientFacts,
        priceNeededForIngredient,
    };
}, {});

// Get a flatted array of nutrients with appropriate base unit of measure
const nutrients = ingredientMap
    .map(({ nutrientFacts }) => {
        return nutrientFacts.map((nutrientFact) =>
            GetNutrientFactInBaseUnits(nutrientFact)
        );
    })
    .flat();

/*
    Reduce to an object with nutrientName as keys.
    `quantityAmount` is totaled for each nutrient with the same name
*/
const nutrientsData = nutrients.reduce((output, nutrient) => {
    if (output[nutrient.nutrientName]) {
        output[nutrient.nutrientName].quantityAmount.uomAmount +=
            nutrient.quantityAmount.uomAmount;
    } else {
        output[nutrient.nutrientName] = nutrient;
    }
    return output;
}, {});

// Get total price of ingredients
const cheapestCost = ingredientMap.reduce((total, current) => {
    return (total += current.priceNeededForIngredient);
}, 0);

// Alphabetize the keys
const nutrientsAtCheapestCost = Object.fromEntries(
    Object.entries(nutrientsData).sort()
);
recipeSummary[cremeBrulee.recipeName] = {
    cheapestCost,
    nutrientsAtCheapestCost,
};

/*
 * YOUR CODE ABOVE THIS, DO NOT MODIFY BELOW
 * */
RunTest(recipeSummary);
